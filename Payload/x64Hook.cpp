// 64 bit is harder than 32 bit
//
// AMD64 Architecture Programmer's Manual Volume 3: 
//  If the operand-size is 32 or 64 bits, the operand is a 16-bit selector followed by a 32-bit offset. 
//  (On AMD64 architecture, 64-bit offset is not supported)
// from: http://ref.x86asm.net/coder64.html#x68
//
// since we can't jump directly like we did in 32 bit use a different technique. 
// Essentially a ROP attack (Return Oriented Programming)
//
//  Prior to branching to the first instruction of the called procedure, the CALL instruction pushes the address in the EIP
//  register onto the current stack.This address is then called the return-instruction pointer and it points to the
//  instruction where execution of the calling procedure should resume following a return from the called procedure.
//  Upon returning from a called procedure, the RET instruction pops the return-instruction pointer from the stack back
//  into the EIP register.Execution of the calling procedure then resumes.
//  The processor does not keep track of the location of the return-instruction pointer.It is thus up to the programmer
//  to insure that stack pointer is pointing to the return-instruction pointer on the stack, prior to issuing a RET instruction.
//  A common way to reset the stack pointer to the point to the return-instruction pointer is to move the contents
//  of the EBP register into the ESP register.If the EBP register is loaded with the stack pointer immediately following
//  a procedure call, it should point to the return instruction pointer on the stack.
//  The processor does not require that the return instruction pointer point back to the calling procedure.Prior to
//  executing the RET instruction, the return instruction pointer can be manipulated in software to point to any address
//  in the current code segment(near return) or another code segment(far return).Performing such an operation,
//  however, should be undertaken very cautiously, using only well defined code entry points.
//
// From:
//      Intel� 64 and IA-32 Architectures
//      Software Developer�s Manual section 6.2.4.2
//  https://software.intel.com/en-us/download/intel-64-and-ia-32-architectures-sdm-combined-volumes-2a-2b-2c-and-2d-instruction-set-reference-a-z
//
// 
// 2.1.3 ModR/M and SIB Bytes
//  Many instructions that refer to an operand in memory have an addressing - form specifier byte(called the ModR / M
//    byte) following the primary opcode.The ModR / M byte contains three fields of information :
//  � The mod field combines with the r / m field to form 32 possible values : eight registers and 24 addressing modes.
//  � The reg / opcode field specifies either a register number or three more bits of opcode information.The purpose
//    of the reg / opcode field is specified in the primary opcode.
//  � The r / m field can specify a register as an operand or it can be combined with the mod field to encode an
//    addressing mode.Sometimes, certain combinations of the mod field and the r / m field are used to express
//    opcode information for some instructions.
//    Certain encodings of the ModR / M byte require a second addressing byte(the SIB byte).The base - plus - index and
//    scale - plus - index forms of 32 - bit addressing require the SIB byte.The SIB byte includes the following fields :
//  � The scale field specifies the scale factor.
//  � The index field specifies the register number of the index register.
//  � The base field specifies the register number of the base register.
//    See Section 2.1.5 for the encodings of the ModR / M and SIB bytes
// Design -
//   push the address of the hook function onto the stack
//   call a return 
//
// C7 /0 id MOV r/m32, imm32 MI Valid Valid Move imm32 to r/m32
// from https://software.intel.com/sites/default/files/managed/a4/60/325383-sdm-vol-2abcd.pdf
// Vol. 2B section: 4-35
//
// imm32 � An immediate doubleword value used for instructions whose operand-size attribute is 32
// bits.It allows the use of a number between +2,147,483,647 and �2,147,483,648 inclusive.
// from: https://software.intel.com/sites/default/files/managed/a4/60/325383-sdm-vol-2abcd.pdf
// Vol. 2B Section 3-5
//
// references:
//    http://www.c-jump.com/CIS77/ASM/Stack/S77_0040_esp_register.htm
//    http://www.c-jump.com/CIS77/CPU/x86/X77_0100_sib_byte_layout.htm
//
#include <Windows.h>
#include <stdint.h>
#include <sstream>

// byte alignment of the structure, specifying no padding
// struct will be the exact number of bytes required
#pragma pack(push, 1)
struct hook64bit
{
    uint8_t  push;
    uint32_t lowerBits; /* lower 32-bits of the address to jump to */
    uint8_t  mov;
    uint8_t  modrm;
    uint8_t  sib;
    uint8_t  offset;
    uint32_t highBits;  /* upper 32-bits of the address to jump to */
    uint8_t  ret;
};
#pragma pack(pop)

namespace x64Hook
{
    struct hook64bit gOrigInstr = { 0 };
    struct hook64bit gFakeInstr = { 0 };
    void* gFuncAddr = NULL;

    // Initial Hook of the function
    // Sets the global variables to allow for unhook and rehook
    // Paramters
    //    hookFnPtr -> pointer to the hook function 
    void ConfigHook(void* hookFnPtr)
    {
        gFuncAddr = CreateRemoteThread;
        DWORD oldProtection;
        // make memory writable. Read only by default
        BOOL protectRes = VirtualProtect(CreateRemoteThread, sizeof(struct hook64bit), PAGE_EXECUTE_READWRITE, &oldProtection);
        if (protectRes)
        {
            struct hook64bit* funcCall = (struct hook64bit *)CreateRemoteThread;

            // save original call + payload so we can revert
            memcpy(&gOrigInstr, funcCall, sizeof(struct hook64bit));
            
            const uint8_t PUSH = 0x68;
            const uint8_t MOV = 0xC7;
            const uint8_t RET = 0xC3;

            // [--][--]+disp8 & EAX
            // 1. The [--][--] nomenclature means a SIB follows the ModR/M byte
            // 3. The disp8 nomenclature denotes an 8-bit displacement that follows the ModR/M byte 
            // (or the SIB byte if one is present) and that is sign - extended and added to the index.
            //
            // https://software.intel.com/sites/default/files/managed/a4/60/325383-sdm-vol-2abcd.pdf
            // Vol 2A. Section 2-6
            //
            // 01  = Mod
            // 000 = Reg
            // 100 = R/M
            const uint8_t MoDRM = 0x44;

            // Scaled Index Byte
            // 00  = Scale
            // 010 = Index = EDX
            // 100 = Base = ESP
            const uint8_t SIB = 0x24;

            // we pushed a 32 bit value already
            // put the next 32 bits into the high DWORD part
            const uint8_t offset = 4;

            // push the lower bits onto the stack
            // at this point we have an invalid stack frame (0xffff)
            funcCall->push = PUSH;
            funcCall->lowerBits = (uint32_t)(uintptr_t)hookFnPtr;

            // using the MOV OP Code with the ModR/M and SIP bytes
            // we combine the high bits with the lower bits 
            // put the value in ESP register
            // this gives us the correct address as the stack frame 
            funcCall->mov = MOV;
            funcCall->modrm = MoDRM;
            funcCall->sib = SIB;
            funcCall->offset = offset;
            funcCall->highBits = (uint32_t)(((uintptr_t)hookFnPtr) >> 32);

            // Ret here will cause a 'return' to the hook function
            funcCall->ret = RET;

            memcpy(&gFakeInstr, funcCall, sizeof(struct hook64bit));
        }
    }

    // ReHook function. 
    // ConfigHook must be called first.
    void HookFunction()
    {
        if ((NULL != gFuncAddr) && (0 != gFakeInstr.mov))
        {
            memcpy(gFuncAddr, &gFakeInstr, sizeof(struct hook64bit));
        }
    }

    // Unhook the API
    // ConfigHook must be called first.
    void UnHookFunction()
    {
        if ((NULL != gFuncAddr) && (0 != gOrigInstr.mov))
        {
            memcpy(gFuncAddr, &gOrigInstr, sizeof(struct hook64bit));
        }
    }
}