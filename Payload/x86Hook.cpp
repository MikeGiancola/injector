#include <Windows.h>
#include <stdint.h>
#include <sstream>

// byte alignment of the structure, specifying no padding
// struct will be the exact number of bytes required
#pragma pack(push, 1)
struct hook32bit
{
    uint8_t opcode;
    int32_t offset;
};
#pragma pack(pop)

namespace x86Hook
{
    struct hook32bit gOrigInstr = { 0 };
    struct hook32bit gFakeInstr = { 0 };
    void* gFuncAddr = NULL;

    // Initial Hook of the function
    // Sets the global variables to allow for unhook and rehook
    // Paramters
    //    hookFnPtr -> pointer to the hook function 
    void ConfigHook(void* hookFnPtr)
    {
        // save memory location so we can use it in fake api
        gFuncAddr = CreateRemoteThread;

        DWORD oldProtection;
        // make memory writable. Read only by default
        BOOL protectRes = VirtualProtect(CreateRemoteThread, sizeof(struct hook32bit), PAGE_EXECUTE_READWRITE, &oldProtection);
        if (protectRes)
        {
            // should be a call instruction with an address payload
            struct hook32bit* funcCall = (struct hook32bit *)CreateRemoteThread;

            // save original call + payload so we can revert
            memcpy(&gOrigInstr, funcCall, sizeof(struct hook32bit));

            intptr_t src_addr = (intptr_t)CreateRemoteThread;
            intptr_t dst_addr = (intptr_t)hookFnPtr;

            // change the instruction to a jump
            const uint8_t jmp = 0xE9;
            funcCall->opcode = (uint8_t)jmp;

            // jump requires relative offset. 
            funcCall->offset = (int32_t)(dst_addr - (src_addr + sizeof(*funcCall)));

            // save fake function call for use in callback API
            memcpy(&gFakeInstr, funcCall, sizeof(struct hook32bit));
        }
    }

    // ReHook function. 
    // ConfigHook must be called first.
    void HookFunction()
    {
        if ((NULL != gFuncAddr) && (0 != gFakeInstr.opcode))
        {
            // swap the call back to original value
            memcpy(gFuncAddr, &gFakeInstr, sizeof(struct hook32bit));
        }
    }

    // Unhook the API
    // ConfigHook must be called first.
    void UnHookFunction()
    {
        if ((NULL != gFuncAddr) && (0 != gOrigInstr.opcode))
        {
            memcpy(gFuncAddr, &gOrigInstr, sizeof(struct hook32bit));
        }
    }

}
