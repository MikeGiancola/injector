#include <Windows.h>
#include <Shlobj.h>
#include <sstream>
#include <string>

#include "Errors.h"
#include "InjectorApp.h"
#include "x86Hook.h"
#include "x64Hook.h"


BOOL gIs32Bit = FALSE;

// Hook function, called instead of the real CreateRemoteThread API
// Will unhook, call the real function, rehook
// Will display a Message box of the pid being connected to
HANDLE WINAPI CreateRemoteThread_fake(_In_ HANDLE hProcess,
                                     _In_opt_ LPSECURITY_ATTRIBUTES lpThreadAttributes,
                                     _In_ SIZE_T dwStackSize,
                                     _In_ LPTHREAD_START_ROUTINE lpStartAddress,
                                     _In_opt_ LPVOID lpParameter,
                                     _In_ DWORD dwCreationFlags,
                                     _Out_opt_ LPDWORD lpThreadId)
{
    if (gIs32Bit)
    {
        x86Hook::UnHookFunction();
    }
    else
    {
        x64Hook::UnHookFunction();
    }

    // call original API to send results back
    DWORD remoteThreadId = 0;
    HANDLE threadHandle = CreateRemoteThread(hProcess,
                                             lpThreadAttributes,
                                             dwStackSize,
                                             lpStartAddress,
                                             lpParameter,
                                             dwCreationFlags,
                                             &remoteThreadId);
    if (NULL != lpThreadId)
    {
        *lpThreadId = remoteThreadId;
    }

    if (gIs32Bit)
    {
        x86Hook::HookFunction();
    }
    else
    {
        x64Hook::HookFunction();
    }

    std::stringstream dialogMsg;
    dialogMsg << "CreateRemoteThread was called in Pid ";
    dialogMsg << GetCurrentProcessId();
    dialogMsg << " TO Pid ";
    dialogMsg << GetProcessId(hProcess);
    ::MessageBoxA(NULL, dialogMsg.str().c_str(), "CreateRemoteThread_fake", MB_OK);
    
    return threadHandle;
}

// Thread main function.
// Thread will find the appropriate location for notepad.exe
// Create a new process (start notepad)
// Create a mutex inside the new notepad process by calling CreateRemoteThread 
// This will trigger the hooked function.
//
// Thread is required due to the fact that you aren't guaranteed
// all APIs will be available in DLLMain.
DWORD WINAPI ThreadMain(LPVOID lpParam)
{
    // sleep for 2 seconds. 
    // Arbitrary value - 
    ::Sleep(2000); 

    std::string exePath;
    PWSTR sys32PathWide = NULL;

    // launch appropriate notepad for our architecture (32 bit or 64)
    if (gIs32Bit)
    {
        SHGetKnownFolderPath(FOLDERID_SystemX86, 0, NULL, &sys32PathWide);
    }
    else
    {
        SHGetKnownFolderPath(FOLDERID_System, 0, NULL, &sys32PathWide);
    }

    // App is in multibyte - not Wide chars (International paths not supported)
    // convert from UNICODE to MBCS
    size_t numConverted = 0;
    char* mbsString = (char*)calloc(1024, 1);
    if (NULL != mbsString)
    {
        if (0 == wcstombs_s(&numConverted, mbsString, 1024, sys32PathWide, _TRUNCATE))
        {
            exePath = mbsString;
        }
        free(mbsString);
    }

    if (!exePath.empty())
    {
        exePath += "\\notepad.exe";
    }

    // Launch Notepad and create a mutex in that process
    STARTUPINFO* startInfo = (STARTUPINFO*)calloc(1, sizeof(STARTUPINFO));
    PROCESS_INFORMATION* procInfo = (PROCESS_INFORMATION*)calloc(1, sizeof(PROCESS_INFORMATION));
    if ((NULL != startInfo) && (NULL != procInfo))
    {
        startInfo->cb = sizeof(STARTUPINFO);
        BOOL createRes = CreateProcessA(NULL, (char*)(exePath.c_str()), NULL, NULL, FALSE, 0, NULL, NULL, startInfo, procInfo);
        if (createRes)
        {
            WaitForInputIdle(procInfo->hProcess, 30000); // max wait of 30 seconds

            LPVOID funcAddr;
            ErrorCodes rval = InjectorApp::FindFuncAddress("kernel32.dll", "CreateMutexA", &funcAddr);
            if (ENoError == rval)
            {
                InjectorApp::InvokeRemoteAPI(procInfo->hProcess, funcAddr, "{F5F23115-C3F2-46DF-9E05-4E67F9B92085}");
            }

            CloseHandle(procInfo->hProcess);
            CloseHandle(procInfo->hThread);
        }
    }

    if (NULL != startInfo)
    {
        free(startInfo);
        startInfo = NULL;
    }
    if (NULL != procInfo)
    {
        free(procInfo);
        procInfo = NULL;
    }
    return 0;
}

// main entry point of the dll
INT APIENTRY DllMain(HMODULE hDll, DWORD reason, LPVOID reserved)
{
    switch (reason)
    {
    case DLL_PROCESS_ATTACH:
    {
        IsWow64Process(GetCurrentProcess(), &gIs32Bit);
        if (gIs32Bit)
        {
            OutputDebugString("[DllMain] Hooking 32 bit.\n");
            x86Hook::ConfigHook(CreateRemoteThread_fake);
        }
        else
        {
            OutputDebugString("[DllMain] Hooking 64 bit.\n");
            x64Hook::ConfigHook(CreateRemoteThread_fake);
        }

        // calling different functions in DllMain is dangerous
        // kick off a thread to run after we're loaded
        DWORD threadId = 0;
        CreateThread(NULL, 0, ThreadMain, NULL, 0, &threadId);
    }
        break;
    case DLL_PROCESS_DETACH:
        break;
    case DLL_THREAD_ATTACH:
        break;
    case DLL_THREAD_DETACH:
        break;
    }
    return TRUE;
}