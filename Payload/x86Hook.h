namespace x86Hook
{
    // Initial Hook of the function
    // Sets the global variables to allow for unhook and rehook
    // Paramters
    //    hookFnPtr -> pointer to the hook function 
    void ConfigHook(void* hookFnPtr);

    // ReHook function. 
    // ConfigHook must be called first.
    void HookFunction();

    // Unhook the API
    // ConfigHook must be called first.
    void UnHookFunction();
}