#include <Windows.h>
#include <pathcch.h>
#include <string>

#include "Errors.h"
#include "InjectorApp.h"
#include "Tester.h"

namespace InjectorApp
{
    ErrorCodes FindFuncAddress(const std::string& dllName, const std::string& apiName, LPVOID* funcAddr)
    {
        ErrorCodes rval = ENoError;
        if ((NULL == funcAddr) || (dllName.empty()) || (apiName.empty()))
        {
            rval = EBadFuncParams;
            printf("[FindFuncAddress] bad params.\n");
            return rval;
        }

        HMODULE kernModule = GetModuleHandleA(dllName.c_str());
        if (NULL == kernModule)
        {
            rval = EModuleHandleFailed;
            const DWORD last = GetLastError();
            printf("[FindFuncAddress] Unable to get handle to %s. GetModuleHandleA failed with code %d (0x%x)\n", dllName.c_str(), last, last);
        }

        LPVOID loadlibAddr = NULL;
        if (ENoError == rval)
        {
            loadlibAddr = (LPVOID)GetProcAddress(kernModule, apiName.c_str());
            if (NULL == loadlibAddr)
            {
                rval = EGetProcAddrFailed;
                const DWORD last = GetLastError();
                printf("[FindFuncAddress] Unable to get address to %s. GetProcAddress failed with code %d (0x%x)\n", apiName.c_str(), last, last);
            }
        }

        if (ENoError == rval)
        {
            *funcAddr = loadlibAddr;
        }
        return rval;
    }

    ErrorCodes InvokeRemoteAPI(HANDLE process, LPVOID funcAddr, const std::string& param)
    {
        ErrorCodes rval = ENoError;

        if ((NULL == process) ||
            (INVALID_HANDLE_VALUE == process) ||
            param.empty() ||
            (NULL == funcAddr))
        {
            rval = EBadFuncParams;
            printf("[InvokeRemoteAPI] bad params.\n");

            return rval;
        }

        LPVOID remoteMemory = NULL;
        if (ENoError == rval)
        {
            remoteMemory = (LPVOID)VirtualAllocEx(process, NULL, param.length(), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
            if (NULL == remoteMemory)
            {
                rval = EVirtualAllocFail;
                const DWORD last = GetLastError();
                printf("[InvokeRemoteAPI] Unable to allocate memory in remote process. VirtualAllocEx failed with code %d (0x%x)\n", last, last);
            }
        }

        if (ENoError == rval)
        {
            const int memWriteRes = WriteProcessMemory(process, remoteMemory, param.c_str(), param.length(), NULL);
            if (0 == memWriteRes)
            {
                rval = EWriteProcMemFail;
                const DWORD last = GetLastError();
                printf("[InvokeRemoteAPI] Unable to write memory in remote process. WriteProcessMemory failed with code %d (0x%x)\n", last, last);
            }
        }

        if (ENoError == rval)
        {
            HANDLE threadId = CreateRemoteThread(process, NULL, 0, (LPTHREAD_START_ROUTINE)funcAddr, remoteMemory, NULL, NULL);
            if (NULL == threadId)
            {
                rval = ECreateRemoteThread;
                const DWORD last = GetLastError();
                printf("[InvokeRemoteAPI] Unable to create remote thread. CreateRemoteThread failed with code %d (0x%x)\n", last, last);
            }
        }

        return rval;
    }

    ErrorCodes GetInjectDllPath(HANDLE process, std::string& dllPath)
    {
        ErrorCodes rval = ENoError;
        if ((NULL == process) || (INVALID_HANDLE_VALUE == process))
        {
            rval = EBadFuncParams;
            printf("[InvokeRemoteAPI] bad params.\n");
            return rval;
        }

        BOOL remoteIs32Bit = FALSE;
        if (0 == IsWow64Process(process, &remoteIs32Bit))
        {
            rval = EIsWow64Process;
            const DWORD last = GetLastError();
            printf("[InvokeRemoteAPI] IsWow64Process failed for remote process with code %d (0x%x)\n", last, last);
        }

        BOOL currIs32Bit = FALSE;
        if (ENoError == rval)
        {
            if (0 == IsWow64Process(GetCurrentProcess(), &currIs32Bit))
            {
                rval = EIsWow64Process;
                const DWORD last = GetLastError();
                printf("[InvokeRemoteAPI] IsWow64Process failed for current process with code %d (0x%x)\n", last, last);
            }
        }

        // either we are both 32 or 64 bit. Otherwise, unsupported configuration
        // address of LoadLibrary could be wrong otherwise.
        rval = EUnsupportedArch;
        if ((remoteIs32Bit && currIs32Bit) || (!remoteIs32Bit && !currIs32Bit))
        {
            rval = ENoError;
        }
        else
        {
            printf("[InvokeRemoteAPI] Architecture mismatch.\n");
        }

        char* binPath = NULL;
        if (ENoError == rval)
        {
            // assume the dll is a sibling to the executable
            binPath = (char*)calloc(256, 1);
            if (NULL == binPath)
            {
                rval = ENoMemory;
                printf("[InvokeRemoteAPI] No memory.\n");
            }
        }

        if (ENoError == rval)
        {
            if (GetModuleFileNameA(NULL, binPath, 256))
            {
                dllPath = binPath;
                dllPath = dllPath.substr(0, dllPath.rfind("\\"));
                dllPath += "\\Payload.dll";
            }
            else
            {
                rval = EGetModuleFilename;
                printf("[InvokeRemoteAPI] GetModuleFileNameA failed!\n");
            }
        }
        if (NULL != binPath)
        {
            free(binPath);
        }
        return rval;
    }

    ErrorCodes InjectAndStart(HANDLE process)
    {
        ErrorCodes rval = ENoError;
        if ((NULL == process) || (INVALID_HANDLE_VALUE == process))
        {
            rval = EBadFuncParams;
            printf("[InjectAndStart] bad params.\n");
            return rval;
        }

        std::string injDllPath;
        rval = GetInjectDllPath(process, injDllPath);

        LPVOID funcAddress = NULL;
        if (ENoError == rval)
        {
            const std::string dllName = "kernel32.dll";
            const std::string APIName = "LoadLibraryA";
            rval = FindFuncAddress(dllName, APIName, &funcAddress);
        }

        if (ENoError == rval)
        {
            rval = InvokeRemoteAPI(process, funcAddress, injDllPath);
        }

        return rval;
    }

    ErrorCodes RunningAsAdmin(bool* isAdmin)
    {
        ErrorCodes rval = ENoError;
        HANDLE hToken = NULL;
        const BOOL openRes = OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY, &hToken);
        if (!openRes)
        {
            rval = EOpenProcFail;
            const DWORD last = GetLastError();
            printf("OpenProcessToken failed with code %d (0x%x)\n", last, last);
        }

        TOKEN_ELEVATION tokenEle;
        if (ENoError == rval)
        {
            DWORD tokenEleSize = sizeof(TOKEN_ELEVATION);
            const BOOL tokenInfoRes = GetTokenInformation(hToken, TokenElevation, &tokenEle, tokenEleSize, &tokenEleSize);
            if (!tokenInfoRes)
            {
                rval = ETokenInfo;
                const DWORD last = GetLastError();
                printf("GetTokenInformation failed with code %d (0x%x)\n", last, last);
            }
        }

        if (ENoError == rval)
        {
            if (0 != tokenEle.TokenIsElevated)
            {
                *isAdmin = true;
            }
        }

        if (hToken)
        {
            CloseHandle(hToken);
        }
        return rval;
    }

    ErrorCodes DoInjection(DWORD procId)
    {
        HANDLE process = INVALID_HANDLE_VALUE;

        ErrorCodes rval = ENoError;        
        process = OpenProcess(PROCESS_ALL_ACCESS, FALSE, procId);
        if (NULL == process)
        {
            rval = EOpenProcFail;
            const DWORD last = GetLastError();
            printf("[DoInjection] Unable to open pid %d. OpenProcess failed with code %d (0x%x)\n", procId, last, last);
        }

        if (ENoError == rval)
        {
            rval = InjectAndStart(process);
        }

        if ((NULL != process) && (INVALID_HANDLE_VALUE != process))
        {
            CloseHandle(process);
        }
        return rval;
    }

}