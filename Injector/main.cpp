#include <Windows.h>
#include <pathcch.h>

#include <string>
#include "Tester.h"
#include "Errors.h"
#include "InjectorApp.h"

// converts the pid string to an integer.
// Parameters:
//  pidStr: input. String representation of the pid number
//  outPid: output. integer version of the pid
// Returns ENoError on success. Otherwise an error code
ErrorCodes GetPidFromParam(const char* pidStr, int* outPid)
{
    ErrorCodes rval = ENoError;
    if ((NULL == pidStr) || (NULL == outPid))
    {
        rval = EBadFuncParams;
        printf("[GetPidFromParam] bad parameters.\n");
        return rval;
    }

    rval = EBadPidParam;
    try
    {
        *outPid = std::stoi(pidStr);
        rval = ENoError;
    }
    catch (const std::invalid_argument&)
    {
        printf("[GetPidFromParam] bad data.\n");
    }
    catch (const std::out_of_range& )
    {
        printf("[GetPidFromParam] bad value.\n");
    }
    return rval;
}


// Returns true / false if the process is running with admin privs.
// Parameter:
//  isAdmin: output. True if the process has admin privileges. False otherwise
// Returns ENoError on success. Otherwise an error code
ErrorCodes RunningAsAdmin(bool* isAdmin)
{
    ErrorCodes rval = ENoError;
    if (NULL == isAdmin)
    {
        rval = EBadFuncParams;
        printf("[RunningAsAdmin] isAdmin parameter is NULL.\n");
    }

    HANDLE hToken = NULL;
    if (ENoError == rval)
    {
        const BOOL openRes = OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY, &hToken);
        if (!openRes)
        {
            rval = EOpenProcFail;
            const DWORD last = GetLastError();
            printf("[RunningAsAdmin] OpenProcessToken failed with code %d (0x%x)\n", last, last);
        }
    }

    TOKEN_ELEVATION tokenEle;
    if (ENoError == rval)
    {
        DWORD tokenEleSize = sizeof(TOKEN_ELEVATION);
        const BOOL tokenInfoRes = GetTokenInformation(hToken, TokenElevation, &tokenEle, tokenEleSize, &tokenEleSize);
        if (!tokenInfoRes)
        {
            rval = ETokenInfo;
            const DWORD last = GetLastError();
            printf("[RunningAsAdmin] GetTokenInformation failed with code %d (0x%x)\n", last, last);
        }
    }

    if (ENoError == rval)
    {
        if (0 != tokenEle.TokenIsElevated)
        {
            *isAdmin = true;
        }
    }
    
    if (hToken) 
    {
        CloseHandle(hToken);
    }
    return rval;
}

// prints basic usage of the app
// no parameters, no return codes
void Usage()
{
    printf("-----------------------------------------------\n");
    printf(" Usage:                                        \n");
    printf("    Injector.exe <pid you wish to inject into> \n");
    printf("                                               \n");
    printf("     *Must be run as an Administrator*         \n");
    printf("-----------------------------------------------\n");
}

// "real" main program. 
// Takes the pid, in string form, of the process where we want to inject and hook
// Using that pid it connects to the process, loads the dll and hooks the api
//
// Parameters:
//    pid: input. Pid of the process we want to inject and hoook (victim proc)
// Returns ENoError on success. Otherwise an error code.
ErrorCodes mainRunner(char* pid)
{
    bool isAdmin = false;
    ErrorCodes rval = RunningAsAdmin(&isAdmin);
    if ((ENoError != rval) || (!isAdmin))
    {
        Usage();
        rval = EBadNumParams;
    }

    int procId = 0;
    if (ENoError == rval)
    {
        rval = GetPidFromParam(pid, &procId);
    }
    
    if (ENoError == rval)
    {
        rval = InjectorApp::DoInjection(procId);
    }

    if (ENoError == rval)
    {
        printf("Completed with no errors.\n");
    }
    else
    {
        printf("Errors occurred. Code: %d\n", rval);
    }

    return rval;
}

// check for 'hidden' command line switch. If passed the
// -test command line parameter, return true otherwise return false
//
// Parameters:
//    param: input. The command line parameter to test
//    runTests: output. True if the -test switch was passed. False otherwise
// Returns: ENoError on success. Error code otherwise.
ErrorCodes CheckIfRunTests(char* param, bool* runTests)
{
    ErrorCodes rval = ENoError;
    if ((NULL == param) || (NULL == runTests))
    {
        rval = EBadFuncParams;
    }

    if (ENoError == rval)
    {
        *runTests = false;
        if ((param[0] && ('-' == param[0])) &&
            (param[1] && ('t' == param[1])) && 
            (param[2] && ('e' == param[2])) &&
            (param[3] && ('s' == param[3])) &&
            (param[4] && ('t' == param[4])))
        {
            *runTests = true;
        }
    }
    return rval;
}

// main entry point.
// Checks number of params, then takes appropriate actions based on the option passed
int main(int argc, char** argv)
{
    ErrorCodes rval = ENoError;
    if (2 != argc)
    {
        Usage();
        rval = EBadNumParams;
    }

    bool runTests = false;
    rval = CheckIfRunTests(argv[1], &runTests);
    if (ENoError == rval)
    {
        if (!runTests)
        {
            rval = mainRunner(argv[1]);
        }
        else
        {
            int numPassed = 0;
            int numFailed = 0;
            Tester::RunTests(&numPassed, &numFailed);
            printf("Results of tests %d passed and %d failed.\n", numPassed, numFailed);
        }
    }

    return rval;
}