#pragma once
//
// Error codes returned from methods
// 
enum ErrorCodes
{
    // No error
    ENoError = 0, 

    // Command line param errors
    EBadNumParams, 
    EBadPidParam,

    // Function parameter checks
    EBadFuncParams,

    EOpenProcFail,
    EModuleHandleFailed,
    EGetProcAddrFailed,
    EVirtualAllocFail,
    EWriteProcMemFail,
    ECreateRemoteThread,
    ENoMemory,
    EGetModuleFilename,
    EIsWow64Process,
    EUnsupportedArch,
    EOpenProcToken,
    ETokenInfo,

    EUnknown
};
