#pragma once
#include <string>
#include "Errors.h"

namespace InjectorApp
{
    // Given the dll and API name, returns the address. Assumes DLL is already loaded in memory
    // Parameters: 
    //    dllName -> input. Name of the dll to be searched
    //    apiName -> input. Name of the API to looking for
    //    funcAddr -> output. Address of the function
    // Returns ENoError on success. Appropriate code on failure.
    ErrorCodes FindFuncAddress(const std::string& dllName, const std::string& apiName, LPVOID* funcAddr);

    // Given the process handle, attempts to invoke the API at funcAddr passing it the 
    // specified parameter.
    // Parameters:
    //    process -> input. Handle to a process. Should be opened with ALL ACCESS
    //    funcAddr -> input. Address of the function to invoke. 
    //    param -> input. Any parameters required to run the function.
    // Returns ENoError on success. Appropriate code on failure.
    ErrorCodes InvokeRemoteAPI(HANDLE process, LPVOID funcAddr, const std::string& param);

    // Given the process handle, return the path to the dll to be loaded into 
    // the other process (victim process). 
    //
    // Assumes the 32 and 64 bit dlls are in the same folder as the executable
    //
    // Parameters:
    //    process -> input. Handle to the remote process
    //    dllPath -> output. Path to the 32 or 64 bit dll
    // Returns ENoError on success. Otherwise an error code.
    ErrorCodes GetInjectDllPath(HANDLE process, std::string& dllPath);

    ErrorCodes InjectAndStart(HANDLE process);
    
    // API to determine if the executable as admin privs
    // Parameters:
    //    isAdmin -> output. True if is admin, False otherwise
    // Returns ENoError on success. Otherwise an error code.
    ErrorCodes RunningAsAdmin(bool* isAdmin);

    // Opens the process specified by the PID then calls InjectAndStart
    // Paramters:
    //     procId -> PID of the process to hook / inject into
    // Returns ENoError on success, Otherwise and error code.
    ErrorCodes DoInjection(DWORD procId);
}
