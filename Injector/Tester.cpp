//
//      Test code
//
// Basic testing of the different APIs used
// 
// RunTests is executed when the -tests command line parameter is passed to the exe
//
#include <Windows.h>
#include <string>
#include "Errors.h"
#include <assert.h>
#include <Shlobj.h>
#include <stdlib.h>
#include <sstream>
#include "InjectorApp.h"

ErrorCodes GetPidFromParam(const char* pidStr, int* outPid);
ErrorCodes mainRunner(char* pid);


namespace Tester
{
    // **************************************
    //      GetPidFromParam
    // **************************************
    bool GetPidFromParam_Test1()
    {
        bool passed = false;
        int pid;
        ErrorCodes rval = GetPidFromParam(NULL, &pid);
        if (ENoError != rval)
        {
            passed = true;
        }
        return passed;
    }

    bool GetPidFromParam_Test2()
    {
        bool passed = false;
        ErrorCodes rval = GetPidFromParam("100", NULL);
        if (ENoError != rval)
        {
            passed = true;
        }
        return passed;
    }

    bool GetPidFromParam_Test3()
    {
        bool passed = false;
        int pid;
        ErrorCodes rval = GetPidFromParam("testme", &pid);
        if (ENoError != rval)
        {
            passed = true;
        }
        return passed;
    }

    // success case
    bool GetPidFromParam_Test4()
    {
        bool passed = false;
        int pid;
        ErrorCodes rval = GetPidFromParam("99", &pid);
        if (ENoError == rval)
        {
            passed = true;
        }
        return passed;
    }
    
    void RunGetPidFromParamTests(int* numPassed, int* numFailed)
    {
        GetPidFromParam_Test1() ? (*numPassed)++ : (*numFailed)++;
        GetPidFromParam_Test2() ? (*numPassed)++ : (*numFailed)++;
        GetPidFromParam_Test3() ? (*numPassed)++ : (*numFailed)++;
        GetPidFromParam_Test4() ? (*numPassed)++ : (*numFailed)++;
    }
    
    // **************************************
    //      FindFuncAddress
    // **************************************
    bool FindFuncAddress_Test1()
    {
        bool passed = false;

        const std::string dllName = "kernel32.dll";
        const std::string APIName = "LoadLibraryA";
        LPVOID funcAddress = NULL;
        ErrorCodes rval = InjectorApp::FindFuncAddress("", APIName, &funcAddress);
        if (ENoError != rval)
        {
            passed = true;
        }
        return passed;
    }

    bool FindFuncAddress_Test2()
    {
        bool passed = false;

        const std::string dllName = "kernel32.dll";
        const std::string APIName = "LoadLibraryA";
        LPVOID funcAddress = NULL;
        ErrorCodes rval = InjectorApp::FindFuncAddress(dllName, "", &funcAddress);
        if (ENoError != rval)
        {
            passed = true;
        }
        return passed;
    }

    bool FindFuncAddress_Test3()
    {
        bool passed = false;

        const std::string dllName = "kernel32.dll";
        const std::string APIName = "LoadLibraryA";
        LPVOID funcAddress = NULL;
        ErrorCodes rval = InjectorApp::FindFuncAddress(dllName, APIName, NULL);
        if (ENoError != rval)
        {
            passed = true;
        }
        return passed;
    }

    bool FindFuncAddress_Test4()
    {
        bool passed = false;

        const std::string dllName = "somethinginvalid.dll";
        const std::string APIName = "LoadLibraryA";
        LPVOID funcAddress = NULL;
        ErrorCodes rval = InjectorApp::FindFuncAddress(dllName, APIName, &funcAddress);
        if (ENoError != rval)
        {
            passed = true;
        }
        return passed;
    }

    bool FindFuncAddress_Test5()
    {
        bool passed = false;

        const std::string dllName = "kernel32.dll";
        const std::string APIName = "somethinginvalid";
        LPVOID funcAddress = NULL;
        ErrorCodes rval = InjectorApp::FindFuncAddress(dllName, APIName, &funcAddress);
        if (ENoError != rval)
        {
            passed = true;
        }
        return passed;
    }

    bool FindFuncAddress_Test6()
    {
        bool passed = false;

        const std::string dllName = "kernel32.dll";
        const std::string APIName = "LoadLibraryA";
        LPVOID funcAddress = NULL;
        ErrorCodes rval = InjectorApp::FindFuncAddress(dllName, APIName, &funcAddress);
        if (ENoError == rval)
        {
            passed = true;
        }
        return passed;
    }

    void RunFindFuncAddressTests(int* numPassed, int* numFailed)
    {
        FindFuncAddress_Test1() ? (*numPassed)++ : (*numFailed)++;
        FindFuncAddress_Test2() ? (*numPassed)++ : (*numFailed)++;
        FindFuncAddress_Test3() ? (*numPassed)++ : (*numFailed)++;
        FindFuncAddress_Test4() ? (*numPassed)++ : (*numFailed)++;
        FindFuncAddress_Test5() ? (*numPassed)++ : (*numFailed)++;
        FindFuncAddress_Test6() ? (*numPassed)++ : (*numFailed)++;
    }

    // **************************************
    //      InvokeRemoteAPI
    // **************************************
    bool InvokeRemoteAPI_Test1()
    {
        bool passed = false;

        LPVOID funcAddress = NULL;        
        const std::string dllName = "kernel32.dll";
        const std::string APIName = "LoadLibraryA";
        // assume success here, test case
        InjectorApp::FindFuncAddress(dllName, APIName, &funcAddress);

        ErrorCodes rval = InjectorApp::InvokeRemoteAPI(NULL, funcAddress, APIName);
        if (ENoError != rval)
        {
            passed = true;
        }
        return passed;
    }

    bool InvokeRemoteAPI_Test2()
    {
        bool passed = false;

        LPVOID funcAddress = NULL;
        const std::string dllName = "kernel32.dll";
        const std::string APIName = "LoadLibraryA";
        // assume success here, test case
        InjectorApp::FindFuncAddress(dllName, APIName, &funcAddress);

        ErrorCodes rval = InjectorApp::InvokeRemoteAPI(INVALID_HANDLE_VALUE, funcAddress, APIName);
        if (ENoError != rval)
        {
            passed = true;
        }
        return passed;
    }

    bool InvokeRemoteAPI_Test3()
    {
        bool passed = false;

        LPVOID funcAddress = NULL;
        const std::string dllName = "kernel32.dll";
        const std::string APIName = "LoadLibraryA";
        // assume success here, test case
        InjectorApp::FindFuncAddress(dllName, APIName, &funcAddress);

        ErrorCodes rval = InjectorApp::InvokeRemoteAPI(GetCurrentProcess(), funcAddress, "");
        if (ENoError != rval)
        {
            passed = true;
        }
        return passed;
    }

    bool InvokeRemoteAPI_Test4()
    {
        bool passed = false;

        LPVOID funcAddress = NULL;
        const std::string dllName = "kernel32.dll";
        const std::string APIName = "LoadLibraryA";
        // assume success here, test case
        InjectorApp::FindFuncAddress(dllName, APIName, &funcAddress);

        ErrorCodes rval = InjectorApp::InvokeRemoteAPI(GetCurrentProcess(), NULL, APIName);
        if (ENoError != rval)
        {
            passed = true;
        }
        return passed;
    }

    void RunInvokeRemoteAPITests(int* numPassed, int* numFailed)
    {
        InvokeRemoteAPI_Test1() ? (*numPassed)++ : (*numFailed)++;
        InvokeRemoteAPI_Test2() ? (*numPassed)++ : (*numFailed)++;
        InvokeRemoteAPI_Test3() ? (*numPassed)++ : (*numFailed)++;
        InvokeRemoteAPI_Test4() ? (*numPassed)++ : (*numFailed)++;
    }

    // **************************************
    //      GetInjectDllPath
    // **************************************
    bool GetInjectDllPath_Test1()
    {
        bool passed = false;
        std::string dllPath;
        ErrorCodes rval = InjectorApp::GetInjectDllPath(NULL, dllPath);
        if (ENoError != rval)
        {
            passed = true;
        }
        return passed;
    }

    bool GetInjectDllPath_Test2()
    {
        bool passed = false;
        std::string dllPath;
        ErrorCodes rval = InjectorApp::GetInjectDllPath(INVALID_HANDLE_VALUE, dllPath);
        if (ENoError != rval)
        {
            passed = true;
        }
        return passed;
    }

    void RunGetInjectDllPathTests(int* numPassed, int* numFailed)
    {
        GetInjectDllPath_Test1() ? (*numPassed)++ : (*numFailed)++;
        GetInjectDllPath_Test2() ? (*numPassed)++ : (*numFailed)++;
    }

    // **************************************
    //      InjectAndStart
    // **************************************
    bool InjectAndStart_Test1()
    {
        bool passed = false;
        ErrorCodes rval = InjectorApp::InjectAndStart(NULL);
        if (ENoError != rval)
        {
            passed = true;
        }
        return passed;
    }

    bool InjectAndStart_Test2()
    {
        bool passed = false;
        ErrorCodes rval = InjectorApp::InjectAndStart(INVALID_HANDLE_VALUE);
        if (ENoError != rval)
        {
            passed = true;
        }
        return passed;
    }

    void RunInjectAndStartTests(int* numPassed, int* numFailed)
    {
        InjectAndStart_Test1() ? (*numPassed)++ : (*numFailed)++;
        InjectAndStart_Test2() ? (*numPassed)++ : (*numFailed)++;
    }

    // **************************************
    //      mainRunner
    // **************************************
    bool MainRunner_Test1()
    {
        bool passed = false;
        ErrorCodes rval = mainRunner("hello");
        if (ENoError != rval)
        {
            passed = true;
        }
        return passed;
    }

    bool MainRunner_Test2()
    {
        bool passed = false;

        // try localsystem pid
        ErrorCodes rval = mainRunner("4");
        if (ENoError != rval)
        {
            passed = true;
        }
        return passed;
    }

    bool MainRunner_Test3()
    {
        bool passed = false;

        ErrorCodes rval = mainRunner("-1");
        if (ENoError != rval)
        {
            passed = true;
        }
        return passed;
    }

    void RunMainRunnerTests(int* numPassed, int* numFailed)
    {
        MainRunner_Test1() ? (*numPassed)++ : (*numFailed)++;
        MainRunner_Test2() ? (*numPassed)++ : (*numFailed)++;
        MainRunner_Test3() ? (*numPassed)++ : (*numFailed)++;
    }

    // **************************************
    //      E2E Tests
    // **************************************
    bool E2ETests_Test1()
    {
        bool passed = false;

        BOOL currIs32Bit = FALSE;
        IsWow64Process(GetCurrentProcess(), &currIs32Bit);

        std::string exePath;
        PWSTR sys32PathWide = NULL;

        // should return correct folder for our arch
        if (currIs32Bit)
        {
            SHGetKnownFolderPath(FOLDERID_SystemX86, 0, NULL, &sys32PathWide);
        }
        else
        {
            SHGetKnownFolderPath(FOLDERID_System, 0, NULL, &sys32PathWide);
        }
        
        size_t numConverted = 0;
        char* mbsString = (char*)calloc(1024, 1);
        if (NULL != mbsString)
        {
            if (0 == wcstombs_s(&numConverted, mbsString, 1024, sys32PathWide, _TRUNCATE))
            {
                exePath = mbsString;
            }
            free(mbsString);
        }

        if (!exePath.empty())
        {
            exePath += "\\notepad.exe";
        }

        STARTUPINFO* startInfo = (STARTUPINFO*)calloc(1, sizeof(STARTUPINFO));
        PROCESS_INFORMATION* procInfo = (PROCESS_INFORMATION*)calloc(1, sizeof(PROCESS_INFORMATION));
        if ((NULL != startInfo) && (NULL != procInfo))
        {
            startInfo->cb = sizeof(STARTUPINFO);
            BOOL createRes = CreateProcessA(NULL, (char*)(exePath.c_str()), NULL, NULL, FALSE, 0, NULL, NULL, startInfo, procInfo);
            if (createRes)
            {
                WaitForInputIdle(procInfo->hProcess, 30000); // max wait of 30 seconds
                CloseHandle(procInfo->hProcess);
                CloseHandle(procInfo->hThread);

                // cheat here to convert DWORD PID to string
                // let STL do the work for me..
                std::stringstream pidStream;
                pidStream << procInfo->dwProcessId;

                std::string foo = pidStream.str();
                
                char* pidStr = (char*)calloc(65, 1); 
                memcpy(pidStr, pidStream.str().c_str(), pidStream.str().length());

                ErrorCodes rval = mainRunner(pidStr);
                if (ENoError == rval)
                {
                    passed = true;
                }
                free(pidStr);
            }
        }

        if (NULL != startInfo)
        {
            free(startInfo);
            startInfo = NULL;
        }
        if (NULL != procInfo)
        {
            free(procInfo);
            procInfo = NULL;
        }
        return passed;
    }

    void RunE2ETests(int* numPassed, int* numFailed)
    {
        E2ETests_Test1() ? (*numPassed)++ : (*numFailed)++;
    }

    // **************************************
    //      Main
    // **************************************
    void RunTests(int* numPassed, int* numFailed)
    {
        RunGetPidFromParamTests(numPassed, numFailed);
        assert((*numFailed) == 0);

        RunFindFuncAddressTests(numPassed, numFailed);
        assert((*numFailed) == 0);

        RunInvokeRemoteAPITests(numPassed, numFailed);
        assert((*numFailed) == 0);

        RunGetInjectDllPathTests(numPassed, numFailed);
        assert((*numFailed) == 0);

        RunInjectAndStartTests(numPassed, numFailed);
        assert((*numFailed) == 0);

        RunMainRunnerTests(numPassed, numFailed);
        assert((*numFailed) == 0);

        RunE2ETests(numPassed, numFailed);
        assert((*numFailed) == 0);
    }
}