# Injector


This tool will inject the built dll into a process (specified at runtime via the command line) which then hooks the CreateRemoteThread api within that process. When the process running the injected dll invokes the CreateRemoteThread API, a message box is displayed with the source and destination Pids. 

The injected DLL  spawns a thread inside the victim process where it runs Notepad.exe, then connects to it and creates a mutex within it (by calling CreateRemoteThread). This mutex creation causes the CreateRemoteThread hook to trigger and display the message box.

You must run the appropriate executable for the architecture of the destination process (32 bit exe  -> 32 bit victim or 64 bit exe to 64 bit victim). The app also requires administrator privileges. The app assumes the dll to be injected into the victim process is in the same folder as the exectuable.

Usage injector.exe `pid` of process you wish to inject into.

## How it works
The injector application opens a handle to the destination process then invokes the LoadLibrary function within that process passing it the path to the hooking dll. When the destination process loads the hooking dll, the CreateRemoteThread is hooked using a bit of assembler (different for 32 and 64 bit).  When the hooked API is called the function is unhooked, the real API is called, results are stored then the function is rehooked. The real results are then returned to the caller.

## How hooking works

### 32 bit Hooking
To hook the function in a 32 bit process, we're able to use a simple jump command and an offset to the function. 

 - Changes permission on the memory to allow it to be written to
 - Copies / stores the original instructions at the function address (a call and an address)
 - Replaces the original instructions with a JMP and the offset between the 2 functions

### 64 bit Hooking
To hook the function in a 64 bit process, we need to use a bit more complicated technique. AMD 64 instruction set doesn't support the same JMP to a 64 bit offset. To get around this, we essentially use a Return Oriented Programming (ROP) design where the address to the hooked function is pushed onto the stack (as if it was a function call) then a RET instruction is issued. Since the return address is the first address always pushed onto the stack, the program will "return" the the hooked function. 

Since the address is 64 bits, the design used is to push the first 32 bits onto the stack using a PUSH instruction. Once this is done, we use a MOV instruction with the addressing - form specifier byte (MoDR/M) and the scaled index bytes (SIB). When the move instruction is used with the modifier, it will move the value into the ESP register (points to the top of the stack). The SIB specified the offset into the register to put the bytes. Since we've already pushed the lower 4 bytes onto the stack, we specify 4 as the SIB to move the 2nd value into the high order bytes.
		

            // 01  = Mod
            // 000 = Reg
            // 100 = R/M
            const uint8_t MoDRM = 0x44;

            // Scaled Index Byte
            // 00  = Scale
            // 010 = Index = EDX
            // 100 = Base = ESP
            const uint8_t SIB = 0x24;

            // we pushed a 32 bit value already
            // put the next 32 bits into the high DWORD part
            const uint8_t offset = 4;

## References:

[Intel® 64 and IA-32 Architectures - Software Developer’s Manual ](https://software.intel.com/sites/default/files/managed/a4/60/325383-sdm-vol-2abcd.pdf)

[AMD64 Architecture Programmer's Manual Volume 3](http://ref.x86asm.net/coder64.html#x68)

[C-Jump Description of ESP Register](http://www.c-jump.com/CIS77/ASM/Stack/S77_0040_esp_register.htm)

[C-Jump Description of SIB Byte](http://www.c-jump.com/CIS77/CPU/x86/X77_0100_sib_byte_layout.htm)

[C-Jump Description of MOD-REG-R/M Byte](http://www.c-jump.com/CIS77/CPU/x86/X77_0060_mod_reg_r_m_byte.htm)

[Swanson Technologies, Understanding Intel Instruction Sizes](https://www.swansontec.com/sintel.html)

[x86 Opcode Instruction Reference](http://ref.x86asm.net/coder64.html#x68)

[Felix Cloutier description of JMP OP](https://www.felixcloutier.com/x86/jmp)

[Felix Cloutier description of MOV OP](https://www.felixcloutier.com/x86/mov)

[subhook implementation by Zeex](https://github.com/Zeex/subhook)

> Written with [StackEdit](https://stackedit.io/).